#!/bin/bash
wget https://s3.amazonaws.com/amazoncloudwatch-agent/amazon_linux/amd64/latest/amazon-cloudwatch-agent.rpm
echo 'agent downloaded'
sudo rpm -U ./amazon-cloudwatch-agent.rpm
echo 'agent installed'
wget https://gitlab.com/JessDevOps/cloudwatchagent/-/raw/main/amazon-cloudwatch-agent.json
echo 'json downloaded from gitlab'
sudo mv /home/ec2-user/amazon-cloudwatch-agent.json /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
echo 'moved file' 
sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -c file:/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
echo 'agent started'
sudo systemctl status amazon-cloudwatch-agent
